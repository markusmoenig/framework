//
//  ViewController.swift
//  Framework
//
//  Created by Markus Moenig on 31.12.18.
//  Copyright © 2018 Markus Moenig. All rights reserved.
//

import Cocoa
import Metal
import MetalKit

class ViewController: NSViewController {

    var app : App!
    var mmView : MMView!

    override func viewDidLoad() {
        super.viewDidLoad()
                
        mmView = view as? MMView
        app = App( mmView )
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}

